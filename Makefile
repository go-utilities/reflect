# use bash
SHELL=/usr/bin/bash

.PHONY: all lint release

all:
	go build .

lint:
	reuse lint
	golangci-lint run 

# (1) adjust version in VERSION file and commit
# (2) create an annotated tag with name RELEASE
# syntax: make release RELEASE=vX.Y.Z
release:
	@if ! [ -z $(RELEASE) ]; then \
		echo $(RELEASE) > ./VERSION; \
		git commit -a -s -m "release $(RELEASE)"; \
		git push; \
		git tag -a $(RELEASE) -m "release $(RELEASE)"; \
		git push origin $(RELEASE); \
	fi